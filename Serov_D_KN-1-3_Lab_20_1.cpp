#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std;

////1////
class vec
{
public:
	int n;	
	int sum();
	int sorting();
	void input();
	void print();
	
private:	
	int e[100];
};


void vec::print()
{
	int i;
	printf("�������� ������� V(%d): \n",n);
	for (i = 0; i < n; i++)
		cout << e[i] << " " ;
	cout<<endl;
}

void vec::input()
{
	int i;
	printf("������ �������� ������� V(%d): \n",n);
	for (i = 0; i < n; i++)
		cin>>e[i];
}

int vec::sum()
{
	int s=0; int i;
	
	for(i = 0; i < n; i++)	
	{
		if(e[i]%2 == 0)
			s = s + i;
	}
return s;
}

int vec::sorting()
{
	int tmp, i, j, k;
	for (i = 1; i < n; i++)
	{
		tmp = e[i];
		k = 0;
		while (tmp>e[k])
			k++;
		for (j = i-1; j > (k-1); j--)
			e[j+1] = e[j];
		e[k] = tmp;
	}	
}
//// //// 

////2////
class mat
{
public:
	int n;
	int num();
	int sorting();
	void random();
	void print();
	
private:	
	int e[100];
};

void mat::random()
{
	srand(time(NULL));
	int i;
	for (i = 0; i < n; i++)
		e[i] = rand()%20-10;
}

void mat::print()
{
	int i;
	printf("�������� ������ �(%d): \n",n);
	for (i = 0; i < n; i++)
		cout << e[i] << " " ;
	cout<<endl;
}


int mat::num()
{
	int N=0; int i;
	
	for(i = 0; i < n; i++)	
	{
		if(e[i] < 0)
			N++;
	}
return N;
}

int mat::sorting()
{
	int tmp, i, j, k;
	for (i = 1; i < n; i++)
	{
		tmp = e[i];
		k = 0;
		while (tmp>e[k])
			k++;
		for (j = i-1; j > (k-1); j--)
			e[j+1] = e[j];
		e[k] = tmp;
	}	
}
//// ////

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	////1////
	vec r1;
	cout<<endl<<" ������ ������� �������� ������� V = ";
	cin>>r1.n;
	r1.input();
	
	r1.print();
	cout<<endl<<" ���� i�����i� ������ �������i� ������� V = "<<r1.sum()<<endl;
	r1.sorting();
	cout<<endl<<" ³������������ ������ V: "<<endl;
	r1.print();
	//// ////

	////2////
	mat r2;
	cout<<endl<<" ������ ������� �������� ����� A = ";
	cin>>r2.n;
	r2.random();
	r2.sorting();
	cout<<endl<<" ³������������ ����� A: "<<endl;
	r2.print();
	cout<<endl<<" ʳ������ �䑺���� �������� ������ A = "<< r2.num() <<endl;
	//// ////


	system("pause");
	return 0;
}
